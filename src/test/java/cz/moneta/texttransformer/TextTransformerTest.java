package cz.moneta.texttransformer;

import cz.moneta.texttransformer.transformer.TextTransformer;
import org.junit.Assert;
import org.junit.Test;

public class TextTransformerTest {

    private final TextTransformer textTransformer = new TextTransformer();

    @Test
    public void testSample1() {
        Assert.assertEquals("?šÁm es kaj ,jOha", textTransformer.transform("Ahoj, jak se máš?"));
    }

    @Test
    public void testSample2() {
        Assert.assertEquals(".NjaF iM ej", textTransformer.transform("Je     mi   fajn."));
    }

}
