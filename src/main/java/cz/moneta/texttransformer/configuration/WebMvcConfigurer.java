package cz.moneta.texttransformer.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        // http://blog.netgloo.com/2015/05/19/spring-boot-avoid-pathvariable-parameters-getting-truncated-on-dots/
        configurer.setUseSuffixPatternMatch(false);
    }
}
