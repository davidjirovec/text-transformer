package cz.moneta.texttransformer.controller;

public class TextTransformation {

    private String transformedText;

    public TextTransformation(String transformedText) {
        this.transformedText = transformedText;
    }

    public String getTransformedText() {
        return transformedText;
    }

    public void setTransformedText(String transformedText) {
        this.transformedText = transformedText;
    }
}
