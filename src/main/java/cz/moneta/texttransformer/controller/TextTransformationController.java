package cz.moneta.texttransformer.controller;

import cz.moneta.texttransformer.transformer.TextTransformer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TextTransformationController {

    private final TextTransformer textTransformer;

    public TextTransformationController(TextTransformer textTransformer) {
        this.textTransformer = textTransformer;
    }

    @RequestMapping("textTransformations/{text}")
    public TextTransformation getTransformation(@PathVariable("text") String text) {
        return new TextTransformation(textTransformer.transform(text));
    }

}
