package cz.moneta.texttransformer.transformer;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class TextTransformer {

    private static final Set<String> UPPERCASE_MARKERS = Sets.newHashSet("a", "e", "i", "o", "u");

    public String transform(String text) {
        final String spaceNormalizedText = StringUtils.normalizeSpace(text);
        final StringBuilder result = new StringBuilder(spaceNormalizedText.length());

        for (int i = 0; i < spaceNormalizedText.length(); i++) {
            final char c = spaceNormalizedText.charAt(spaceNormalizedText.length() - i - 1);
            result.append(
                    UPPERCASE_MARKERS.contains(
                            StringUtils.stripAccents(String.valueOf(spaceNormalizedText.charAt(i))))
                            ? Character.toUpperCase(c)
                            : Character.toLowerCase(c)
            );
        }

        return result.toString();
    }

}
