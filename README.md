# Text Transformer

Build

```
mvn clean install
```

Run

```
java -jar target/text-transformer-0.0.1-SNAPSHOT.jar
```

Call example for "Ahoj, jak se máš?"

```
curl -X GET http://localhost:8080/textTransformations/Ahoj%2C%20jak%20se%20m%C3%A1%C5%A1%3F
```

Call example for "Je     mi   fajn."

```
curl -X GET http://localhost:8080/textTransformations/Je%20%20%20%20%20mi%20%20%20fajn.
```